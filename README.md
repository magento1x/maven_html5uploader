# Maven_Html5uploader

Originaly posted here: https://www.mavenecommerce.com/2016/10/18/magento-html5-uploader/

Why We Decided to Develop a HTML5 Uploader?
UPDATE: as Magento 1.x reached end-of-support on June 30, 2020, all M1 extensions were removed from Marketplace on July 7, 2020. But we still take care of partners who decided not to migrate to Magento 2 for now. So if you still need our Magento HTML5 Uploader extension, you can download it from this page (find the link in the bottom). In case you still didn’t migrate your Magento 1.x website to Magento 2.x, take a look at our post.

 
For some time now, popular web browsers are limiting the use of Flash in favor of HTML5. Why everyone hates Flash so much? The main concern lies in its security level. Because Flash is a closed, proprietary system on a web that deserves open standards. It’s a popular punching bag for hackers, which puts users at risk over and over again.

For many years Adobe has been trying to improve Flash Player security level, but no matter what they do (and did) it never seems enough. New vulnerabilities appear faster than the old ones are fixed. Security is not the only characteristic where HTML5 outruns Flash. HTML5 is fully responsive and is supported by all browsers and devices. It’s also has a lighter code, uses less CPU, and is SEO Friendly (while search engines hate Flash). Also, there is no need to install any third-party tools to view it (you need Adobe Flash Player to view Flash). Out of the box Magento uses outdated image uploader which is based on Flash. Firefox doesn’t support it anymore (you can’t upload images in Firefox) while Google Chrome is going to stop supporting Flash technology in the nearest future. As a result, you won’t be able to upload images for products or CMS content in modern browsers. Because this issue was one of the biggest concerns for Magento users for the past few months, Maven decided to develop an alternative method of uploading images and CMS content to Magento based stores. Our solution is based on HTML5 technology, which is modern, stable and won’t get outdated in the nearest future so you won’t be forced to look for another solution for a long time. Our HTML5 Uploader is bug-free, easy to install and use and allows you simultaneously upload multiple images both for products and CMS content in convenient way. Moreover, it’s 100% free.

## HTML5 Uploader Features
- 100% Free
- Easy installation
- Multiple image types support
- Multiupload functionality
- Drag and Drop interface
- Thumbnail generation
- Upload progress indicator
- Customizable image names
- Customizable storage path
- Large image support
- Developer friendly (Full source code included)
 
## Installation Process
As we mentioned, installation process is extremely easy. Just follow the following steps to enable HTML5 Uploader:
1- Install HTML5 Uploader from Magento Connect
2- Go to System – Configuration – MAVENECOMMERCE – Html5Uploader – General Settings
3- Set “Enable” to “Yes”
4- Clear cache.
That’s it. Now you have HTML5 uploader instead of a Flash uploader.  

## How it works
The process of uploading a product image looks like this:
![](https://gitlab.com/magento1x/maven_html5uploader/-/raw/master/maven1.png)
HTML5 Uploader drag and dropAs you can see it’s an easy to use drag-and-drop interface.
![](https://gitlab.com/magento1x/maven_html5uploader/-/raw/master/maven2.png)
Successfully uploaded images look like this:HTML5 Uploader SuccessUploading images for CMS content is also that easy:
![](https://gitlab.com/magento1x/maven_html5uploader/-/raw/master/maven3.png)
![](https://gitlab.com/magento1x/maven_html5uploader/-/raw/master/maven4.png)
HTML5 uploader media storageHTML5 uploader Media Storage successHere, at Maven, we already use HTML5 uploader on the majority of our clients’ projects as a more convenient way for uploading product and CMS content images. We hope you will like the way it functions as well.

[Download HTML5 Uploader from our site](https://gitlab.com/magento1x/maven_html5uploader/-/raw/master/mavenecommerce-HTML5_Uploader-1.0.9.tgz)
